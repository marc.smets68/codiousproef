﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef.Data
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MyProperty { get; set; }
        public List<Contact> Contacts { get; set; }
        public Adres Adres { get; set; }
    }
}
