﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef.Data
{
    public class Consultant
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public decimal Uurtarief { get; set; }
        public string Email { get; set; }
        public Adres Adres { get; set; }

    }
}
