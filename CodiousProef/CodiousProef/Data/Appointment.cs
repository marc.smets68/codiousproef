﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodiousProef.Data
{
    public class Appointment
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public Consultant Consultant { get; set; }
        public DateTime Time { get; set; }
        public string Description { get; set; }
    }
}
