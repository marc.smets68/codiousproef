﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CodiousProef.Repositories;
using CodiousProef.Data;

namespace CodiousProef.Controllers
{
    public class ConsultantController : Controller
    {
        private IConsultantRepository _consultantRepository;

        public ConsultantController(IConsultantRepository consultantRepository)
        {
            _consultantRepository = consultantRepository;
        }
        // GET: Consultant
        public ActionResult Index()
        {
            var consultants = _consultantRepository.GetAllConsultants();
            return View(consultants);
        }

        // GET: Consultant/Details/5
        public ActionResult Details(int id)
        {
            var consultant = _consultantRepository.GetConsultant(id);
            return View(consultant);
        }

        // GET: Consultant/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Consultant/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var consultant = new Consultant();
                consultant.Name = collection["Name"];
                consultant.Uurtarief = Convert.ToDecimal(collection["Uurtarief"]);
                consultant.Email = collection["Email"];
                _consultantRepository.CreateConsultant(consultant);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Consultant/Edit/5
        public ActionResult Edit(int id)
        {
            var consultant = _consultantRepository.GetConsultant(id);
            return View(consultant);
        }

        // POST: Consultant/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                var consultant = _consultantRepository.GetConsultant(id);
                consultant.Name = collection["Name"];
                consultant.Uurtarief = Convert.ToDecimal(collection["Uurtarief"]);
                consultant.Email = collection["Email"];
                _consultantRepository.UpdateConsultant(consultant);


                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Consultant/Delete/5
        public ActionResult Delete(int id)
        {
           
            return View();
        }

        // POST: Consultant/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}